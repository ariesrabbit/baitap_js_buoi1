// BÀI TẬP 1
// Input : số ngày làm
// 
// Step: 
// *s1: tạo ra biến chứa số ngày làm
// *s2: tạo ra biến lưu tiền lương một ngày (100.000)
//  *s2: lấy số ngày làm * tiền lương một ngày 
// 
// Out put : Số tiền lương 

var dayWork = 30;
var salaryPerday = 100000;
var salaryTotal;
salaryTotal = dayWork*salaryPerday;
console.log('Bài 1: Tiền lương nhận được sau 30 ngày làm việc: ', salaryTotal);

// BÀI TẬP 2
// Input : 5 số thực bất kì
// 
// Step: 
//  *s1: tạo biến lưu 5 số đã nhập
//  *s2: tính tổng của 5 số trên
//  *s3: lấy tổng đó chia cho 5
// 
// Out put : Trung bình cộng 5 số đã nhập

var a,b,c,d,e;
a = 9; b=10; c=11; d=12; e=13;
var Tong;
var TBC;
Tong = a+b+c+d+e;
TBC = Tong / 5;
console.log('Bài 2: Trung bình cộng của 9, 10, 11, 12, 13 là: ', TBC);

// BÀI TẬP 3
// Input : Nhập vào số tiền USD
// 
// Step: 
// *s1: tạo biến chứa số tiền nhập
// *s2: tạo biến lưu giá trị của 1USD ~ 23500VND 
// *s3: lấy số tiền đã nhập * cho 
// 
// Out put : Số tiền sau khi quy đổi vnd

var usd = 5;
var vndFor1usd = 23500;
var quyDoi;
quyDoi = usd*vndFor1usd;
console.log('Số tiền khi quy đổi 5$ sang VND: ', quyDoi);

// BÀI TẬP 4
// Input : Nhập vào chiều dài, chiều rộng hình chữ nhật
// 
// Step: 
// *s1: tạo biến lưu giá trị chiều dài, chiều rộng 
// *s2: Tính chu vi = ( chiều dài + chiều rộng ) * 2
// *s3: Tính diện tích = chiều dài * chiều rộng
// 
// 
// Out put : Giá trị chu vi, diện tích

var chieuDai =65;
var chieuRong =45;
var chuVi, dienTich;
chuVi = (chieuDai + chieuRong) * 2;
dienTich = chieuDai*chieuRong;
console.log(`HCN chiều dài 65, chiều rộng 45, có chu vi là`, chuVi ,`diện tích là`,dienTich);

// BÀI TẬP 4
// Input : Nhập vào một số có 2 chữ số
// 
// Step: 
// *s1: tạo biến lưu giá trị của số vừa nhập
// *s2: Lấy số hàng đơn vị : số vừa nhập%10
// *s3: Lấy số hàng chục : số vừa nhập/10
// *s4: Tính tổng 2 ký số
// 
// Out put : Tổng 2 ký số

var so = 79;
var hangDonvi, hangChuc, tongKyso;
hangDonvi = so%10;
hangChuc = Math.floor(so/10) ; 
tongKyso = hangChuc + hangDonvi;
console.log('Tổng hai ký số của 79 là: ', tongKyso);

